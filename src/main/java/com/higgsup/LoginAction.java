package com.higgsup;

import com.opensymphony.xwork2.ActionSupport;

/**
 * Created by Asus on 11/29/2017.
 */
public class LoginAction extends ActionSupport{

    private UserDao userDao;
    private User user;

    public UserDao getUserDao() {
        return userDao;
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String execute() {
        if (userDao.checkLogin(user)) {
            return SUCCESS;
        }
        else return ERROR;
    }
}
